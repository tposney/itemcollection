## 11.3.0
* As of dnd5e 3.0 there is an implementation of item containers that includes much of the functionality of itemcollection. Accordingly itemcollection will be retired - probably with the release of dnd5e 3.2.x.
* This release is intended to allow you to migrate to the new dnd5e item containers with minimal hassle. 
* With this release itemcollection containers in dnd5e 3.0 are fully functional, set the Item Sheet property to ItemSheet5eWithBags (existing itemcollection containers should just open with the correct sheet). Only existing itemcollection containers are supported, you cannot create new ones nor can you convert dnd5e 3.0 containers to itemcollection containers.
* It is **strongly** reccomended that when migrating a container you move all items from an existing itemcollection container to a new dnd5e 3.0 container. There is a lot of data stored in itemcollection containers that is not used by the dnd5e containers and will just slow things down.
  - One way to migrate containers is to create a similarly named dnd5e 3.0 container, put the itemcollection container inside the newly created container and do export all. 
  - This only works for dnd5e 3.0 containers owned by an actor **NOT** sidebar/compendium containers. Create a dummy actor to hold other containers during migration, the put them wherever you want.
  - An itemcollection container placed inside an unowned dnd5e 3.0 container does not support the import/export buttons.
  - Unlike dnd5e 3.0 containers items in itemcollection containers **DO NOT** apply their passive effects to the actor even if equipped.
  - You cannot edit active effects for items in an itemcollection container.

## 11.0.5
* Fix for longstanding container weight sometimes not updating. Cause of the problem was non item container backpacks in an item container backpack causing confusion.

## 11.0.4
* **Breaking** Change to hooks.call("createItemData", itemData, options, user) -> hooks.call("createItemData", parentItem, itemData, options, user)
  - the item that is being modified is now passed as the first arg.

## 11.0.3
* package fix

## 11.0.2
* release with the new build scripts

## 11.0.1
* Fix for backpack sheet not being set correctly during initialisation.

## 11.0.0
* V11 compatibility and still v10 compatible.
* A change in foundry v11 expand object restricts the depth of objects means that you can have at most 2 levels of bags, i.e. and actor can have a bag of holding that contains a backpack, but the backpack cannot contain another bag.

## 10.0.11
* remove debug left in

## 10.0.10
* Fix for some item weight displays being of the form 0.0130000000001 instead of 0.013.
* Fix for some typos in en.json
* Fix for sheet not opening (failing validation of object).
* Fix for not being able to set bag as having capacity in items.
* Allow item-delete-check to report items added to bags - requires item-delete-check 10.0.8 or later

## 10.0.9
* Fix for edits to items inside bags not being reflected when dragging/exporting from bags.
* **Breaking** Bag capacity, base price, weightless contents, weightless unequipped can now only be edited by GM.
* Fix for weightless unequipped not being stored on updates.

## 10.0.8
* Fix for inadvertant bug that causes sheets to not display.

## 10.0.7
* Fix for changes to item prices in dnd5e 2.1.x.
Changes should be backwards compatible with 2.0.3 but have not tested in detail. So only upgrade if you have moved to dnd5e 2.1.x

## 10.0.6
* [BREAKING] Added bag property "Weightless Unequipped", which when set means that unequipping the bag will set it's weight to 0, otherwise the bag's total weight will contribute to the actors encumberance. Setting the property to true is equivalent to the current default behaviour.
* Added ko-fi link for donations https://ko-fi.com/tposney if you want to support the module financially.

## 10.0.5
* Added ability to disable showing currency on bags.
* Fix for inventory display when bags contain bags....
* [Breaking] Fix for the long standing items disappearing when drag start from a bag. Now will only delete the item if dropped (successfully) on another Item or Actor.

## 10.0.4
* Fix for item.getChatData being async.
* Fix for calc weight typo in v10 update
* Fix for occasional problems when exporting all losing item description.
* Added inventory filter (by name).

## 10.0.3
* Fix icons for sample items compendium
* Remove some dependency warnings

## 10.0.2
* Fix for editing pre v10 containers failing to migrate correctly.
* Fixed some compatability warnings.
* Allow editing of item name.

## 10.0.1
* Prerelease of item containers for v10.
* **NOT** ready for game use, but seems to work.
* Work required on the sheet templates

## 1.8.20
* Don't overwrite details if already set

## 1.8.19
* Allow items to be edited (broken after dnd5e 1.6.2)

## 1.8.18
* respect game settings to not include currency weight when calculating bag weight.
* realign the headings to look a little better.

## 1.8.17
* Added fr.json

## 1.8.16
* Fix for importing items.

## 1.8.14
* Fix for foundry 9.231 induced error.

### 1.8.13
* reinstate special items ("Lantern", "Hempen Rope", "Silk Rope", "Mess Kit", "Bedroll") not affecting backpack capacity, with the logic that they are tied to the outside of the backpack. They still impact the weight of the backpack.

### 1.8.12
* remove debug left in

### 1.8.11
* Added display of bags in bags for contents display.

### 1.8.10
* Changed default tab on item collection sheet to be the contents sheet, rather than the description sheet.
* New setting "Display Description". When true the item description is displayed in the inventory and chat data. When false the contents of the item is displayed instead, showing name, quantity and weight. You can't edit items from this display, but you can at least see what is in the bag.

### 1.8.9
* Fix for currency display
* Fix currency display in shops as well.
* Put back functionality so that itemcollection bags that are not equipped weigh nothing.

### 1.8.8
* Fix for foundry 0.9.226+
* Import All/Export All enabled in foundry 0.9+


### 1.8.7
* Fix for dnd5e 1.5+ to fix currency display in bags.

## 1.8.6
* Fix for libwrapper conflict warning with Variant Encumbrance.
* For foundry 0.9 removed restriction on import/export all.

## 1.8.5
* Fix for a deprecation warning.
* Added Itemcollection.createSpellbookFromActor(actor) which will create a new world item with all the spells from the designated actor.
  * Call if from the console via Itemcollection.createSpellbookFromActor(game.actors.getName(" Name of the actor to use))
  * create a macro with 
  ```
  Itemcollection.createSpellBookFromActor(token.actor)
  ```
  which will create a spellbook for the selected token.

## 1.8.4
seems broken
## 1.8.3 
version bump for 0.8.8

## 1.8.2
* Disable import/export all due to occasional data corruption - will re-enable once fixed

## 1.8.1
* An easy way to create item shops. For example:
```
Itemcollection.Shops.createShop("dnd5e.items", "Legendary Shop", {"rarity": "Legendary"}, {minQuantity: 1, createShop: true})
```
Will create a fully populated shop item with all the legendary items from the compendium dnd5e.items.
The syntax is 
* Itemcollection.Shops.createShop(source-compendium-name, shop-name, filters, options)
  - source-compendium-name is any compendium you have that has items in it, e.g. dnd5e.items
  - shop-name is the name of the shop. If the option createShop is true a new shop will be created, otherwise the items are appended to the existing shop.
  - filters are any/all of
    type: "weapon" or any other item type  
    consumableType: "potion" (or any of the consumable subtypes)  
    equipmentType: "light", or any of the other armor types  
    maxPrice: 1000, no items that cost more than this will be included  
    rarity: "Common" or any of the rarity options (Common, Uncommon, Rare.....)  
    nameRegExp: /[Pp]otion/, a regular expression that the name must match.
      A item must pass ALL the filters for it to be included in the shop.
  - options are any/all of
    createShop: true/false. If true a new shop is created, if false the items are appended to the existing shop
    minQuantity: number create items with a quantity of at least number.
    minValue: number - a total of "number" gold pieces worth of the item are created.
Some examples:
```js
Itemcollection.Shops.createShop("dnd5e.items", "Potion Shop", {type: "consumable", consumableType: "potion"}, {minQuantity: 1, createShop: true})
```
will create a shop with one of every potions in it.
Create a shop with all the common swords in it
```js
Itemcollection.Shops.createShop("dnd5e.items", "Sword Shop", {nameRegExp: /[Ss]word/, rarity: "Common"}, {minQuantity: 1, createShop: true})
```
Add in all the uncommon swords.
```js
Itemcollection.Shops.createShop("dnd5e.items", "Sword Shop", {nameRegExp: /[Ss]word/, rarity: "Uncommon"}, {minQuantity: 1, createShop: false})
```
Now add all the rare swords
```js
Itemcollection.Shops.createShop("dnd5e.items", "Sword Shop", {nameRegExp: /[Ss]word/, ratrity: "Rare"}, {minQuantity: 1, createShop: false})
```
The sample Potion Shop was created with:
```
Itemcollection.Shops.createShop("dnd5e.items", "Potion Shop", {rarity: "Common", nameRegExp: /[Pp]otion/}, {minQuantity: 2, minValue: 100, createShop: true})
Itemcollection.Shops.createShop("dnd5e.items", "Potion Shop", {rarity: "Uncommon", nameRegExp: /[Pp]otion/}, {minQuantity: 2, minValue: 200, createShop: false})
Itemcollection.Shops.createShop("dnd5e.items", "Potion Shop", {rarity: "Rare", nameRegExp: /[Pp]otion/}, {minQuantity: 2, minValue: 200, createShop: false})
```
The General Store was created via
```js
Itemcollection.Shops.createShop("dnd5e.items", "General Store", {rarity: "Common"}, {minQuantity: 10, minValue: 10, createShop: true})
```

## 1.8.0
How to migrate once you've installed the update and all your items has disappeared.
Backup your world and then form the console (or a macro) run
```
await Itemcollection.migrateWorld()
```
foundry 0.8.6 compatibility update.
* Item import/export is much faster now.
* All item export also exports currency to the parent (actor or item);
* Weight/gold value update correctly when items are changed, even if not being edited.
* Capacity for items works correctly.
* Only bakcpacks can be item containers. Make sure you enable the sheet for the item to be able to add/remove items.
* Item containers behave like any other item and can be dragged/dropped to actors/work/compendia.
* Item containers can contain other item containers so you can have nested bags.
* Item shop got an overhaul as well - purchasing auto decrements the cost from players inventory.

* Simplified settings. Bag weight and bag price are the only two itemcollection specific settings. Bag weight is the empty weight of the bag. Itemcollection also uses the dnd5e settings, capacity, weightless contents, and capacity type.

* You are best off creating bags via the ItemSheet5eWithBags editor, but once done they can be used much like other items, say you create a backpack called "bag" and give it to the actor "test" and edit with ItemSheet5eWithBags or ItemSheetShop.

* This has been pretty much a complete rewrite of item containers for foundry 0.8.x and I think it works much more cleanly/consistently now.

* A number of migration functions are provided: (A note before migrating - BACKUP YOUR WORLD FIRST - I have run the conversion scripts on my world without it breaking but your mileage may vary)
    await Itemcollection.migrateWorld() - migrates backpacks for all actors, tokens and world items  - this is the one to do it ally.
    Itemcollection.migrateItems(item collection) - The specific set of items 
    - e.g. await Itemcollection.migrateItems(game.items);  
    Itemcollection.migrateActorItems(actor) - migrate the items for a specific actor  
    Itemcollection.migrateAllActorItems() - migrate items for all world actors  
    Itemcollection.migrateAllTokenItems() - migrate items for all unlinked tokens.  
    Itemcollection.migrateAllItems() - migrate all the world items  

Items work mostly like any other embedded collection, except that they are stored in an item flag.

```js
item = game.actors.getName("test").items.getName("bag")
itemToInsert = game.items.getName("Arrows")
item.createEmbeddedDocuments("Item", [itemToInsert.data])
containedItem = item.items.getName("Arrows")
containedItem.uuid
// returns (for example) "Actor.WlOopcsUtThmw4gy.Item.Z6dElT9pzKR1MDNr.Item.kx25jbgap2eixgf2"
itemToDelete = await fromUuid(conatinedItem.uuid)
itemToDelete.delete()
```
* Known Bugs - none so far, but they are certain to be there.

## 1.3.4
updated to ts project/include libWrapper
Requires uninstall and reinstall
